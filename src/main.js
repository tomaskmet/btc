import Vue from 'vue';
import App from './App.vue';
import VueResource from 'vue-resource';
import lodash from 'lodash';
import VueLodash from 'vue-lodash';

Vue.use(VueResource);
Vue.use(VueLodash, lodash);

new Vue({
  el: '#app',
  render: h => h(App)
})
